# Tutor Guides

This is a list of tutoring guides for high school students.  Any guide in this
document will be general enough that any student at any level should be able to
benefit from the information, regardless of skill level.  Achieving this
requires that we clarify what the guides are trying to do:

* Having an internal Locus of Control
* Active Study Habits
* Organizing activities and life

These skills are not simple, and require practice.  Ultimately, there should be
examples of how to use each skill listed, with a real world example, if
possible.

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img
alt="Creative Commons License" style="border-width:0"
src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />This work
is licensed under a <a rel="license"
href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons
Attribution-NonCommercial 4.0 International License</a>.
